FROM registry.fedoraproject.org/fedora:35

# Setting any of the "...VERSION" to empty string excludes that
# particular module from building.
# If you want to go with the most current version from git, the version
# to go with is mostly "HEAD".
# (synApps is downloaded from a different location as tarball, see
# https://www.aps.anl.gov/BCDA/synApps/Where-to-find-it
# and https://epics.anl.gov/bcda/synApps/tar/synApps_6_1.tar.gz	for example.)
#
# The last version of ASYN known to work as of 2022-04-27 is a664519f42c7b5314dc3367d9654e7e83e48ad4d.
# Note that ASYN has a habit of introducing breaking changes to the
# build system from commit to commit, so sticking to one particular
# version and *not* advancing with HEAD is good advice.
#
# The last version of BASE known to work is HEAD of 2022-04-27.
#
# The latest release of synApps available as of this writing (2022-04-28)
# is 6_1, but it's not necessarily a hard dependency of areaDetector.
# 
ENV \
	AD_EPICS_BASE_VERSION="R7.0.6.1" \
	AD_EPICS_ASYN_VERSION="R4-42" \
	AD_EPICS_SYNAPPS_VERSION="" \
	AD_EPICS_SSCAN_VERSION="HEAD" \
	AD_EPICS_SNCSEQ_VERSION="2.2.9" \
        AD_EPICS_CALC_VERSION="HEAD" \
        AD_EPICS_AUTOSAVE_VERSION="HEAD" \
        AD_EPICS_BUSY_VERSION="HEAD" \
        AD_EPICS_IOCSTATS_VERSION="HEAD" \
	AD_EPICS_PREFIX=/opt/epics

RUN \
    dnf groupinstall -y \
	"Development Tools" \
	"Development Libraries" && \
	dnf -y install \
	g++ \
	rpcgen \
	libtirpc-devel \
	perl-FindBin \
	wget \
	re2c \
	netcdf-devel \
	libtiff-devel \
	zlib-devel \
	hdf5-devel \
	libjpeg-turbo-devel \
	libxml2-devel \
	nexus-devel \
	GraphicsMagick-devel \
	GraphicsMagick-c++ \
	opencv-devel \
	blosc-devel \
	libX11-devel \
	libXext-devel



# Installs epicsBase
RUN \
    if [ ! -z "$AD_EPICS_BASE_VERSION" ]; then \
        cd /tmp && \
        git clone --recurse-submodules https://github.com/epics-base/epics-base.git && \
        cd /tmp/epics-base && \
        git checkout $AD_EPICS_BASE_VERSION && \
        cp configure/CONFIG_SITE ./CONFIG_SITE.orig && \
        cat CONFIG_SITE.orig \
        | sed "s|^\([# ]*\)INSTALL_LOCATION=\([^=]*\)$|INSTALL_LOCATION=$AD_EPICS_PREFIX/base|g" \
        > configure/CONFIG_SITE && \
	make ; \
    fi

# If synApps is not installed, these mandatory modules must be
# installed for areaDetector:
# - CALC
# - SNCSEQ
# - SSCAN

# Install Epics-SNCSEQ
RUN \
    if [ ! -z "$AD_EPICS_SNCSEQ_VERSION" ]; then \
	cd /tmp && \
	wget -O sncseq.tar.gz https://www-csr.bessy.de/control/SoftDist/sequencer/releases/seq-$AD_EPICS_SNCSEQ_VERSION.tar.gz && \
	tar xf sncseq.tar.gz && \
	cd seq-$AD_EPICS_SNCSEQ_VERSION && \
	touch configure/CONFIG_SITE.local && \
	echo "INSTALL_LOCATION=$AD_EPICS_PREFIX/support/sncseq" >> configure/CONFIG_SITE.local && \
	touch configure/RELEASE.local && \
	echo "EPICS_BASE=$AD_EPICS_PREFIX/base" >> configure/RELEASE.local && \
	make && \
	cp configure/RULES_SNCSEQ $AD_EPICS_PREFIX/support/sncseq/configure ;\
    fi

# Install Epics-SSCAN
RUN \
    if [ ! -z "$AD_EPICS_SSCAN_VERSION" ]; then \
	cd /tmp && \
	git clone --recurse-submodules https://github.com/epics-modules/sscan.git && \
	cd /tmp/sscan && \
	git checkout $AD_EPICS_SSCAN_VERSION && \
	\
	touch configure/RELEASE.local && \
	echo "SUPPORT=$AD_EPICS_PREFIX/support" >> configure/RELEASE.local && \
	echo "EPICS_BASE=$AD_EPICS_PREFIX/base" >> configure/RELEASE.local && \
	echo "SNCSEQ=$AD_EPICS_PREFIX/support/sncseq" >> configure/RELEASE.local && \
	\
	touch configure/CONFIG_SITE.local && \
	echo "INSTALL_LOCATION=$AD_EPICS_PREFIX/support/sscan" >> configure/CONFIG_SITE.local && \
	make ; \
    fi

# Install Epics-CALC
RUN \
    if [ ! -z "$AD_EPICS_CALC_VERSION" ]; then \
	cd /tmp && \
	git clone --recurse-submodules https://github.com/epics-modules/calc.git && \
	cd /tmp/calc && \
	git checkout $AD_EPICS_CALC_VERSION && \
	\
	touch configure/RELEASE.local && \
	echo "SUPPORT=$AD_EPICS_PREFIX/support" >> configure/RELEASE.local && \
	echo "SSCAN=$AD_EPICS_PREFIX/support/sscan" >> configure/RELEASE.local && \
	echo "EPICS_BASE=$AD_EPICS_PREFIX/base" >> configure/RELEASE.local && \
	echo "SNCSEQ=$AD_EPICS_PREFIX/support/sncseq" >> configure/RELEASE.local && \
	\
	touch configure/CONFIG_SITE.local && \
	echo "INSTALL_LOCATION=$AD_EPICS_PREFIX/support/calc" >> configure/CONFIG_SITE.local && \
	make ; \
    fi

# Install epics module asyn
RUN \
    if [ ! -z "$AD_EPICS_ASYN_VERSION" ]; then \
        cd /tmp && \
        git clone --recurse-submodules https://github.com/epics-modules/asyn.git && \
        cd /tmp/asyn && \
        git checkout $AD_EPICS_ASYN_VERSION && \
        \
        cd /tmp/asyn/asyn/vxi11 && \
        rpcgen vxi11core.rpcl && \
        rpcgen vxi11intr.rpcl && \
        cd /tmp/asyn && \
        \
        cp configure/RELEASE RELEASE.orig && \
        cp configure/CONFIG_SITE CONFIG_SITE.orig && \
        \
        cat RELEASE.orig \
        | sed "s|^\([# ]*\)SUPPORT=\(.*\)$|SUPPORT=$AD_EPICS_PREFIX/support|g" \
        | sed "s|^\([# ]*\)EPICS_BASE=\(.*\)$|EPICS_BASE=$AD_EPICS_PREFIX/base|g" \
        | sed "s|^\([# ]*\)\(SNCSEQ=\)\(.*\)$|\# \2\3|g" \
        | sed "s|^\([# ]*\)\(SSCAN=\)\(.*\)$|\# \2\3|g" \
        | sed "s|^\([# ]*\)\(CALC=\)\(.*\)$|\# \2\3|g" \
        > configure/RELEASE && \
        \
	cat CONFIG_SITE.orig \
	| sed "s|^\([# ]*\)INSTALL_LOCATION=\([^=]*\)$|INSTALL_LOCATION=$AD_EPICS_PREFIX/support/asyn|g" \
        | sed "s|^\(.*\)TIRPC=\(.*\)$|TIRPC=YES|g" \
        > configure/CONFIG_SITE && \
        \
	make ;\
    fi

# Install Epics-AUTOSAVE (needed for areaDetector ADSim...)
RUN \
    if [ ! -z "$AD_EPICS_AUTOSAVE_VERSION" ]; then \
        cd /tmp && \
        git clone --recurse-submodules https://github.com/epics-modules/autosave.git && \
        cd /tmp/autosave && \
        git checkout $AD_EPICS_AUTOSAVE_VERSION && \
        touch configure/CONFIG_SITE.local &&\
        echo "INSTALL_LOCATION=$AD_EPICS_PREFIX/support/autosave" >> configure/CONFIG_SITE.local && \
        \
        touch configure/RELEASE.local &&\
        echo "SUPPORT=$AD_EPICS_PREFIX/support" >> configure/RELEASE.local && \
        echo "EPICS_BASE=$AD_EPICS_PREFIX/base" >> configure/RELEASE.local && \
        \
        make ;\
    fi

# Install Epics-BUSY (needed for areaDetector ADSim...)
RUN \
    if [ ! -z "$AD_EPICS_BUSY_VERSION" ]; then \
        cd /tmp && \
        git clone --recurse-submodules https://github.com/epics-modules/busy.git && \
        cd /tmp/busy && \
        git checkout $AD_EPICS_BUSY_VERSION && \
        touch configure/CONFIG_SITE.local &&\
        echo "INSTALL_LOCATION=$AD_EPICS_PREFIX/support/busy" >> configure/CONFIG_SITE.local && \
        \
        touch configure/RELEASE.local &&\
        echo "SUPPORT=$AD_EPICS_PREFIX/support" >> configure/RELEASE.local && \
        echo "AUTOSAVE=$AD_EPICS_PREFIX/support/autosave" >> configure/RELEASE.local && \
        echo "BUSY=$AD_EPICS_PREFIX/support/busy" >> configure/RELEASE.local && \
        echo "ASYN=$AD_EPICS_PREFIX/support/asyn" >> configure/RELEASE.local && \
        echo "EPICS_BASE=$AD_EPICS_PREFIX/base" >> configure/RELEASE.local && \
        \
        make ;\
    fi

# Install Epics-DEVIOCSTATS (needed for areaDetector ADSim...)
RUN \
    if [ ! -z "$AD_EPICS_BUSY_VERSION" ]; then \
        cd /tmp && \
        git clone --recurse-submodules https://github.com/epics-modules/iocStats.git && \
        cd /tmp/iocStats && \
        git checkout $AD_EPICS_IOCSTATS_VERSION && \
        touch configure/CONFIG_SITE.local &&\
        echo "INSTALL_LOCATION=$AD_EPICS_PREFIX/support/deviocstats" >> configure/CONFIG_SITE.local && \
        \
        touch configure/RELEASE.local &&\
        echo "SUPPORT=$AD_EPICS_PREFIX/support" >> configure/RELEASE.local && \
        echo "SNCSEQ=$AD_EPICS_PREFIX/support/sncseq" >> configure/RELEASE.local && \
        echo "EPICS_BASE=$AD_EPICS_PREFIX/base" >> configure/RELEASE.local && \
        \
        make ;\
    fi


# FIXME: This isn't finished yet, so synApps will probably not install.
# The deal is that it has a giant list of modules it depends on in
# configure/RELEASE, and sifting through all of them is probably
# unnecessary.
#RUN \
#    if [ ! -z "$AD_EPICS_SYNAPPS_VERSION" ]; then \
#	cd /tmp && \
#	wget https://epics.anl.gov/bcda/synApps/tar/synApps_"$AD_EPICS_SYNAPPS_VERSION".tar.gz -o synApps.tar.gz && \
#	tar xf synApps.tar.gz && \
#	cd /tmp/synApps_"$AD_EPICS_SYNAPPS_VERSION"/support && \
#	cp configure/CONFIG_SITE CONFIG_SITE.orig && \
#	cp configure/RELEASE RELEASE.orig && \
#	cp Makefile Makefile.orig && \
#	\
#	cat RELEASE.orig \
#       | sed "s|^\([# ]*\)SUPPORT=\(.*\)$|SUPPORT=$AD_EPICS_PREFIX/support|g" \
#	| sed "s|^\([# ]*\)EPICS_BASE=\(.*\)$|EPICS_BASE=$AD_EPICS_PREFIX/base|g" \
#	> configure/RELEASE
#	\
#	cat CONFIG_SITE.orig \
#	| sed "s|^\([# ]*\)INSTALL_LOCATION=\([^=]*\)$|INSTALL_LOCATION=$AD_EPICS_PREFIX/support/synApps|g" \
#       > configure/CONFIG_SITE && \
#	\
#	make release && \
#	make
#   fi

# The main meal: areaDetector from https://github.com/areaDetector/areaDetector
# Also downloads the ADSimDetector for learning / testing purposes.
# ENV AD_AREA_DETECTOR_VERSION="HEAD"
#
# Optional Epics modules:
# DEVIOCSTATS
# ALIVE
# ADPLUGINEDGE
# RECCASTER


RUN \
	cd /tmp && \
	git clone --recurse-submodules https://github.com/areaDetector/areaDetector && \
	cd areaDetector && \
	cp configure/EXAMPLE_RELEASE.local                  configure/RELEASE.local && \
	cp configure/EXAMPLE_RELEASE_LIBS.local             configure/RELEASE_LIBS.local && \
	cp configure/EXAMPLE_RELEASE_PRODS.local            configure/RELEASE_PRODS.local && \
	cp configure/EXAMPLE_CONFIG_SITE.local              configure/CONFIG_SITE.local && \
	cp configure/EXAMPLE_CONFIG_SITE.local.linux-x86_64 configure/CONFIG_SITE.local.linux-x86_64 && \
	\
	cp configure/RELEASE_LIBS.local       RELEASE_LIBS.local.orig && \
	cp configure/RELEASE_PRODS.local      RELEASE_PRODS.local.orig && \
	cp configure/CONFIG_SITE.local        CONFIG_SITE.local.orig && \
	cp configure/CONFIG_SITE.local.linux-x86_64 CONFIG_SITE.local.linux-x86_64.orig && \
	\
	cp configure/RELEASE                  RELEASE.orig && \
	cp configure/RELEASE.local            RELEASE.local.orig && \
	cp configure/CONFIG_SITE              CONFIG_SITE.orig && \
	\
	cat RELEASE_LIBS.local.orig \
	| sed "s|^\([# ]*\)\(INSTALL_LOCATION_APP\)=\(.*\)$|\2=/opt/areaDetector|g" \
	| sed "s|^\([# ]*\)\(SUPPORT\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support|g" \
	| sed "s|^\([# ]*\)\(ASYN\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/asyn|g" \
	| sed "s|^\([# ]*\)\(AREA_DETECTOR\)=\(.*\)$|\2=/tmp/areaDetector|g" \
	| sed "s|^\([# ]*\)\(EPICS_BASE\)=\(.*\)$|\2=$AD_EPICS_PREFIX/base|g" \
	> configure/RELEASE_LIBS.local && \
	\
	cat RELEASE_PRODS.local.orig \
	| sed "s|^\([# ]*\)\(INSTALL_LOCATION_APP\)=\(.*\)$|\2=/opt/areaDetector|g" \
	| sed "s|^\([# ]*\)\(SUPPORT\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support|g" \
	| sed "s|^\([# ]*\)\(ASYN\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/asyn|g" \
	| sed "s|^\([# ]*\)\(AREA_DETECTOR\)=\(.*\)$|\2=/tmp/areaDetector|g" \
        | sed "s|^\([# ]*\)\(EPICS_BASE\)=\(.*\)$|\2=$AD_EPICS_PREFIX/base|g" \
        | sed "s|^\([# ]*\)\(AUTOSAVE\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/autosave|g" \
        | sed "s|^\([# ]*\)\(BUSY\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/busy|g" \
        | sed "s|^\([# ]*\)\(CALC\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/calc|g" \
        | sed "s|^\([# ]*\)\(SSCAN\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/sscan|g" \
        | sed "s|^\([# ]*\)\(SNCSEQ\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/sncseq|g" \
        | sed "s|^\([# ]*\)\(DEVIOCSTATS\)=\(.*\)$|\2=$AD_EPICS_PREFIX/support/deviocstats|g" \
	> configure/RELEASE_PRODS.local && \
	\
	cat CONFIG_SITE.local.orig \
	| sed "s|^\([# ]*\)\(WITH_BOOST\)=\(.*\)$|\2=NO|g" \
	| sed "s|^\([# ]*\)\(WITH_PVA\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(WITH_BOOST\)=\(.*\)$|\2=NO|g" \
	| sed "s|^\([# ]*\)\(BLOSC_EXERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(BITSHUFFLE_EXTERNAL\)=\(.*\)$|\2=NO|g" \
	| sed "s|^\([# ]*\)\(GRAPHICSMAGICK_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(HDF5_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(JPEG_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(NETCDF_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(NEXUS_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(OPENCV_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(TIFF_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(XML2_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	| sed "s|^\([# ]*\)\(ZLIB_EXTERNAL\)=\(.*\)$|\2=YES|g" \
	> configure/CONFIG_SITE.local && \
	\
	cat RELEASE.local.orig \
	| sed "s|^\([# ]*\)\(ADPILATUS\)=\(.*\)$|\2=\3|g" \
	| sed "s|^\([# ]*\)\(ADFFMPEGSERVER\)=\(.*\)$|\2=\3|g" \
	| sed "s|^\([# ]*\)\(ADFFMPEGVIEWER\)=\(.*\)$|\2=\3|g" \
	| sed "s|^\([# ]*\)\(ADSIMDETECTOR\)=\(.*\)$|\2=\3|g" \
	| sed "s|^\([# ]*\)\(ADCSIMDETECTOR\)=\(.*\)$|\2=\3|g" \
	> configure/RELEASE.local && \
	\
	make

# Cleaning up
RUN \
    for i in \
	/tmp/epics-base \
	/tmp/asyn \
	/tmp/calc* \
	/tmp/sscan* \
        /tmp/sncseq* \
        /tmp/autosave* \
        /tmp/busy* \
	/tmp/seq* \
	/tmp/synApps* \
	/tmp/areaDetector* \
	/tmp/AD* \
	; \
    do rm -rf "$i" || rm -rf "$i" ; done

