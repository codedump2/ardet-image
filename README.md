AreaDetector Container Image
============================

This is the definition for an [AreaDetector](https://github.com/areaDetector/areaDetector)
container image that can be used with Podman or Docker.
